# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2, Action3
from mud.events import EatEvent, EatWithEvent

class EatAction(Action2):
    EVENT = EatEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "eat"

class EatWithAction(Action3):
	EVENT = EatWithEvent
	ACTION = "eat-with"
	RESOLVE_OBJECT = "resolve_for_operate"
	RESOLVE_OBJECT2 = "resolve_for_use"
